This module allows to grant or remove roles by simply clicking a link or html element.
Which roles are removed or granted depends on the attributes:
data-add-role od data-remove-role.

You can also use the href attribute to perform a redirect after adding or removing a role.

It is also possible to add/remove more than one role by simply
separate multiple roles by a comma.

Important note:
Visit: /admin/config/people/click_a_role and make sure to only allow certain roles to be added or removed by clicking
an element.

Otherwise it would be possible for attackers to get any role they want.
By default this module prevents users from getting any role.

Examples:

Add single role:
<div data-add-role="example_role">Grant <em>example role</em></div>

Remove single role:
<div data-remove-role="example_role">Remove <em>example role</em></div>

Add multiple roles:
<div data-add-role="example_role,second_role,third_role">Grant multiple roles
</div>

Remove multiple roles:
<div data-remove-role="example_role,second_role,third_role">Remove multiple
roles</div>

It is also possible to add and remove roles at the same element:
<div data-add-role="example_role,second_role"
data-remove-role="third_role,fourth_role">Remove multiple roles</div>
