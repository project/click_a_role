<?php

namespace Drupal\click_a_role\Controller;

use Drupal\user\Entity\User;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * An example controller.
 */
class ClickARoleController extends ControllerBase {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $account, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
    $this->configFactory = $config_factory->get('click_a_role.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * Adds given roles to the current user.
   */
  public function setRoles(Request $request) {
    $roles = $this->getRolesFromRequest($request);
    $user = $this->entityTypeManager->getStorage('user')->load($this->account->id());
    foreach ($roles->addRoles as $role) {
      if ($this->handlingOfRoleIsAllowed($role)) {
        $user->addRole($role);
      }
    }
    foreach ($roles->removeRoles as $role) {
      if ($this->handlingOfRoleIsAllowed($role)) {
        $user->removeRole($role);
      }
    }
    $user->save();
    return new JsonResponse([
      'data' => $roles,
      'method' => 'GET',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getRolesFromRequest(Request $request) {
    $content = $request->getContent();
    $roles = json_decode($content);
    return $roles;
  }

  /**
   * {@inheritdoc}
   */
  public function handlingOfRoleIsAllowed($role) {
    $roles = $this->configFactory->get('roles');
    return in_array($role, $roles);
  }

}
