(function ($, Drupal) {
  var initialized;

  function initClickARole() {
    if(!initialized) {
      initialized = true;
      $(document).on('click', '*[data-add-role]', function () {
        setRoles($(this));
        return false;
      });
      $(document).on('click', '*[data-remove-role]', function () {
        setRoles($(this));
        return false;
      });
    }
  }

  function setRoles(element) {
    if(element.data('add-role') !== undefined) {
      var rolesToAdd = element.data('add-role').split(',');
    }
    if(element.data('remove-role') !== undefined) {
      var rolesToRemove = element.data('remove-role').split(',');
    }
    if(element.attr('href')) {
      var href = element.attr('href');
    }
    if(element.data('href')) {
      var href = element.data('href');
    }

    $.ajax({
      type: 'POST',
      url: '/click_a_role/set',
      data: JSON.stringify({ addRoles: rolesToAdd, removeRoles: rolesToRemove }),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: function () {
        if(href) {
          window.location.href = href;
        }
      }
    });
  }

  Drupal.behaviors.clickARole = {
    attach: function (context, settings) {
      initClickARole();
    }
  };
})(jQuery, Drupal);
